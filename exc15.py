myList = ['a', 'b', 'c', 'c', 'f', 's', 'f']
n=3
newList = [item for item in myList for i in range(n)]

print(newList)



output:
    ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c', 'c', 'c', 'c', 'f', 'f', 'f', 's', 's', 's', 'f', 'f', 'f']

		