from itertools import chain

listOfLists =[6, [7,[8,4],9],5]

def flatten(nestedList):
    def aux(listOrItem):
        if isinstance(listOrItem, list):
            for elem in listOrItem:
                for item in aux(elem):
                    yield item
        else:
            yield listOrItem
    return list(aux(nestedList))

print(flatten(listOfLists))



output:
    [6, 7, 8, 4, 9, 5]