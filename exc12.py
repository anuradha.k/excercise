myList = [[2, 'a'], [1, 'g'], 't', [2, 'g'], [3, 'a'], [2, 'l'], [5, 'h']]


def decode(alist):
        def aux(g):
            if isinstance(g, list): 
            	return [(g[1], range(g[0]))]
            else: 
            	return [(g, [0])]
        return [x for g in alist for x, R in aux(g) for i in R]

print(decode(myList))




output:
    ['a', 'a', 'g', 't', 'g', 'g', 'a', 'a', 'a', 'l', 'l', 'h', 'h', 'h', 'h', 'h']