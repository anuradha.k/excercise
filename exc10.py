from itertools import groupby

myList = ['a','a','g', 't','g','g','a','a','a','l','l','h','h','h','h','h']

def encode(alist):
        
    return [[len(list(group)), key] for key, group in groupby(alist)]


print("encoded list",encode(myList))






#output:[[2, 'a'], [1, 'g'], [1, 't'], [2, 'g'], [3, 'a'], [2, 'l'], [5, 'h']]


