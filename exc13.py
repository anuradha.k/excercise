from itertools import groupby

myList = ['a','a','g', 't','g','g','a','a','a','l','l','h','h','h','h','h']


def encode_direct(alist):
    def aux(k, g):
        l = len(list(g))
        if l>1: 
        	return [l, k]
        else:
        	return k
    return [aux(key, group) for key, group in groupby(alist)]

print(encode_direct(myList))



output:
    [[2, 'a'], 'g', 't', [2, 'g'], [3, 'a'], [2, 'l'], [5, 'h']]
