from itertools import groupby

myList = [2,2,6,7,6,6,3,2,2,2,5,5,6,4,4,4,4]

print([list(group) for key, group in groupby(myList)])





output:
    [[2, 2], [6], [7], [6, 6], [3], [2, 2, 2], [5, 5], [6], [4, 4, 4, 4]]


