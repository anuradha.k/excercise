myList = ['a', 'b', 'c', 'c', 'f', 's', 'f']
n=2
newList = [item for item in myList for i in range(n)]

print(newList)


output:
    ['a', 'a', 'b', 'b', 'c', 'c', 'c', 'c', 'f', 'f', 's', 's', 'f', 'f']

		