from itertools import groupby

myList = ['a','a','g', 't','g','g','a','a','a','l','l','h','h','h','h','h']



def encode_modified(alist): 
    return [[len(list(group)), key] if alist.count(key)>1 else key for key, group in groupby(alist)] 
        
    

print(encode_modified(myList))




output:
    [[2, 'a'], [1, 'g'], 't', [2, 'g'], [3, 'a'], [2, 'l'], [5, 'h']]
